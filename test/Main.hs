module Main (main) where

import Test.HUnit (Test(..), runTestTTAndExit)
import GEODE.Metadata.TestPrimaryKey (testPrimaryKey)
import GEODE.Metadata.TestEntry (testEntry)

testMetadata :: Test
testMetadata = TestLabel "Metadata suite" $
  TestList [ testPrimaryKey, testEntry ]

main :: IO ()
main = runTestTTAndExit testMetadata
