{-# LANGUAGE OverloadedStrings #-}
module GEODE.Metadata.TestPrimaryKey (testPrimaryKey) where

import Data.Csv (ToNamedRecord(..))
import Data.Foldable (toList)
import Data.HashMap.Strict ((!?))
import GEODE.Metadata (Book(..), PrimaryKey(..))
import GEODE.Metadata.TSV.Header (getHeader, for)
import Test.HUnit (Test(..), (~?=))
import Test.HUnit.Extra (isJust)

testPrimaryKey :: Test
testPrimaryKey = TestLabel "Testing the PrimaryKey data type" $
  TestList [ testToNamedRecord ]

testToNamedRecord :: Test
testToNamedRecord = TestLabel "Testing ToNamedRecord instance" $
  TestList [ has3Keys, validDefaultHeader ]
  where
    has3Keys = length aNamedRecord ~?= 3
    validDefaultHeader = TestList . toList $
      (isJust . (aNamedRecord !?)) <$> getHeader (for :: PrimaryKey)
    aNamedRecord = toNamedRecord aPrimaryKey

aPrimaryKey :: PrimaryKey
aPrimaryKey = PrimaryKey LGE 1 1212 -- ALCALA DE HÉNARÈS
