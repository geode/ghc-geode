{-# LANGUAGE OverloadedStrings #-}
module GEODE.Metadata.TestEntry (testEntry) where

import Data.Csv (ToNamedRecord(..))
import Data.Foldable (toList)
import Data.HashMap.Strict ((!?))
import Data.Text (dropEnd)
import GEODE.Metadata (evalSplit, newEntry, normalize)
import GEODE.Metadata.Entry (Entry(..))
import GEODE.Metadata.TSV.Header (getHeader, for)
import Test.HUnit (Test(..), (~?=))
import Test.HUnit.Extra (isJust)

testEntry :: Test
testEntry = TestLabel "Testing the Entry data type" $
  TestList [ testToNamedRecord, testNormalize, testEntryConstructor ]

testToNamedRecord :: Test
testToNamedRecord = TestLabel "Testing ToNamedRecord instance" $
  TestList [ has3Keys, validDefaultHeader ]
  where
    has3Keys = length aNamedRecord ~?= 3
    validDefaultHeader = TestList . toList $
      (isJust . (aNamedRecord !?)) <$> getHeader (for :: Entry)
    aNamedRecord = toNamedRecord anEntry

testNormalize :: Test
testNormalize = TestLabel "Testing function normalize" . TestList $
  check <$> [ ("", "")
            , ("é", "é")
            , (headWord anEntry, dropEnd 2 $ name anEntry) ]
  where
    check (a, b) = normalize a ~?= b

testEntryConstructor :: Test
testEntryConstructor = TestLabel "Testing the entry constructor" . TestList $
  [ a0 ~?= "a-0"
  , a1 ~?= "a-1"
  , b0 ~?= "b-0" ]
  where
    (a0, a1, b0) = evalSplit $ (,,)
      <$> (name <$> newEntry "A")
      <*> (name <$> newEntry "A")
      <*> (name <$> newEntry "B")

anEntry :: Entry
anEntry = Entry "ALCALA DE HÉNARÈS" "alcaladehénarès-0" 1212
