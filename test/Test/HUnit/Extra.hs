module Test.HUnit.Extra
  ( isJust
  , isNothing ) where

import Test.HUnit (Test(..), assertBool)
import qualified Data.Maybe as Maybe (isJust, isNothing)

isJust :: Maybe a -> Test
isJust = TestCase . assertBool "Expected a Just" . Maybe.isJust

isNothing :: Maybe a -> Test
isNothing = TestCase . assertBool "Expected a Nothing" . Maybe.isNothing
