(use-modules ((gnu packages haskell-xyz) #:select (ghc-cassava
                                                   ghc-optparse-applicative))
             ((gnu packages haskell-check) #:select (ghc-hunit))
             ((guix build-system haskell) #:select (haskell-build-system))
             ((guix git-download) #:select (git-predicate))
             ((guix gexp) #:select (local-file))
             ((guix licenses) #:select (gpl3+))
             ((guix packages) #:select (origin package)))

(let
  ((%source-dir (dirname (current-filename))))
  (package
    (name "ghc-geode")
    (version "devel")
    (source
      (local-file %source-dir
          #:recursive? #t
          #:select? (git-predicate %source-dir)))
    (build-system haskell-build-system)
    (inputs (list ghc-cassava ghc-optparse-applicative ghc-hunit))
    (home-page "https://gitlab.liris.cnrs.fr/geode/ghc-geode")
    (synopsis "Data structures and tooling used in project GEODE")
    (description
      "Library providing a representation for corpus metadata and primitives to
define command-line tools to process them.")
    (license gpl3+)))
