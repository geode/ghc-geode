# Revision history for ghc-geode

## 0.2.0.0 -- 2023-07-17

* Add unit tests
* Add optparse-applicative parsers for input / output parameters
* Rework metadata types and the way they compose (splitting Article into
  PrimaryKey and Entry)

## 0.1.0.0 -- 2023-05-23

* Exposing some basic stuff required by recent changes in [EDdA-Clinic](https://gitlab.huma-num.fr/alicebrenon/EDdAClinic)
