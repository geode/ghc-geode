{-# LANGUAGE OverloadedStrings #-}
module GEODE.Metadata
  ( module Contrastive
  , module Entry
  , module PrimaryKey
  , module SplitContext
  , module TSV
  , module TSV_Header
  , module Types
  , groupBy
  , indexBy
  , sortBy ) where

import Data.Foldable as Foldable (toList)
import Data.List (sortOn)
import Data.Map.Strict as Map (Map, alter, empty, insert, toList)
import GEODE.Metadata.Contrastive as Contrastive
import GEODE.Metadata.Entry as Entry
  ( Entry(headWord, name, page), newEntry, normalize )
import GEODE.Metadata.PrimaryKey as PrimaryKey
import GEODE.Metadata.SplitContext as SplitContext hiding (get, page, rank)
import GEODE.Metadata.TSV as TSV
import GEODE.Metadata.TSV.Header as TSV_Header
  ( DefaultFields(..), HasDefaultHeader(..), WithDefaultHeader(..), glue )
import GEODE.Metadata.Types as Types

sortBy :: (Foldable t, Ord k) => (a -> k) -> t a -> [a]
sortBy field = sortOn field . Foldable.toList

groupBy :: (Foldable t, Ord k) => (v -> k) -> t v -> [(k, [v])]
groupBy field = Map.toList . foldr group Map.empty
  where
    group article = Map.alter (Just . maybe [article] (article:)) (field article)

indexBy :: (Foldable t, Ord k) => (a -> k) -> t a -> Map k a
indexBy f = foldr (\a -> Map.insert (f a) a) Map.empty
