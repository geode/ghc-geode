{-# LANGUAGE ExplicitNamespaces, FlexibleInstances, MultiParamTypeClasses, TypeOperators #-}
module GEODE.Metadata.Types
  ( Has(..)
  , type (@)(..) ) where

import Data.Csv (FromNamedRecord(..), ToNamedRecord(..))
import Data.HashMap.Strict (union)

infixr 9 @
infixr 9 :@:
data a @ b = a :@: b

class Has a b where
  get :: b -> a

instance Has a a where
  get = id

instance Has a c => Has a (b @ c) where
  get (_ :@: c) = get c

instance {-# OVERLAPS #-} Has a (a @ b) where
  get (a :@: _) = a

instance (ToNamedRecord a, ToNamedRecord b) => ToNamedRecord (a @ b) where
  toNamedRecord (a :@: b) = union (toNamedRecord a) (toNamedRecord b)

instance (FromNamedRecord a, FromNamedRecord b) => FromNamedRecord (a @ b) where
  parseNamedRecord nr = (:@:) <$> parseNamedRecord nr <*> parseNamedRecord nr
