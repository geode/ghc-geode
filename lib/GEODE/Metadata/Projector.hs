{-# LANGUAGE ConstraintKinds #-}
module GEODE.Metadata.Projector
  --( FromBook(..)
  --, FromTome(..) 
  ( HasAuthors(..) 
  , HasDomains(..) ) where
  --, InFile(..)
  --, Named(..)
  --, TXMText
  --, Unique(..) ) where

import GEODE.Metadata.Types (Authors(..), Domains(..))
--import GEODE.Metadata.Types (Authors(..), Book, Domains(..))
import Data.Text (Text)

{-
class Unique a where
  uid :: a -> String

class FromBook a where
  book :: a -> Book

class FromTome a where
  tome :: a -> Int

class Named a where
  name :: a -> Text
-}

class HasAuthors a where
  authors_ :: a -> Authors

  authors :: a -> [Text]
  authors = getAuthors . authors_

class HasDomains a where
  domains_ :: a -> Domains

  domains :: a -> [Text]
  domains = getDomains . domains_

{-
class InFile a where
  relativePath :: a -> String -> FilePath
-}

--type TXMText a = (Unique a, FromBook a, HasAuthors a, HasDomains a)
