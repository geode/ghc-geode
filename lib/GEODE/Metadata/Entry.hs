{-# LANGUAGE DeriveGeneric, FlexibleContexts, OverloadedStrings #-}
module GEODE.Metadata.Entry
  ( Entry(..)
  , newEntry
  , normalize ) where

import Data.Char (isAlphaNum, isSpace, isUpper, toLower)
import Data.Csv (FromNamedRecord(..), ToNamedRecord(..))
import Data.Text as Text (Text, concat, foldl', pack, snoc)
import GEODE.Metadata.SplitContext (Field(..), SplitContext(..), next)
import GEODE.Metadata.TSV.Header
  (DefaultFields(..), HasDefaultHeader(..), HasDefaultHeader(..))
import GHC.Generics (Generic)

data Entry = Entry
  { headWord :: Text
  , name :: Text 
  , page :: Int } deriving (Generic, Show)

instance FromNamedRecord Entry
instance ToNamedRecord Entry

instance HasDefaultHeader Entry where
  defaultFields = DefaultFields [ "headWord", "name", "page" ]

normalize :: Text -> Text
normalize = Text.foldl' appendIf mempty
  where
    appendIf tmpText newChar
      | isSpace newChar = tmpText
      | isUpper newChar = tmpText `snoc` toLower newChar
      | isAlphaNum newChar = tmpText `snoc` newChar
      | otherwise = tmpText `snoc` '-'

newEntry :: SplitContext m => Text -> m Entry
newEntry headWord = do
  count <- Text.pack . show <$> next (HeadWord prefix)
  Entry headWord (Text.concat [prefix, "-", count]) <$> get Page
  where
    prefix = normalize headWord
