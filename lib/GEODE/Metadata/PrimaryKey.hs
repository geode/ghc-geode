{-# LANGUAGE DeriveGeneric, FlexibleContexts, FlexibleInstances, NamedFieldPuns, OverloadedStrings #-}
module GEODE.Metadata.PrimaryKey
  ( Book(..)
  , PrimaryKey(..)
  , relativePath
  , uid ) where

import Data.Csv (FromField(..), FromNamedRecord(..), ToNamedRecord(..), ToField(..))
import GEODE.Metadata.Types (Has(..))
import GEODE.Metadata.TSV.Header (DefaultFields(..), HasDefaultHeader(..))
import GHC.Generics (Generic)
import System.FilePath ((</>), (<.>))
import Text.Printf (printf)

data Book = EDdA | LGE | Wikipedia deriving (Eq, Ord, Read, Show)

instance FromField Book where
  parseField "EDdA" = pure EDdA
  parseField "LGE" = pure LGE
  parseField "Wikipedia" = pure Wikipedia
  parseField _ = mempty

instance ToField Book where
  toField = toField . show

data PrimaryKey = PrimaryKey
  { book :: Book
  , tome :: Int
  , rank :: Int } deriving (Eq, Ord, Generic, Show)

instance FromNamedRecord PrimaryKey
instance ToNamedRecord PrimaryKey

uid :: Has PrimaryKey a => a -> String
uid a = printf "%s_%d_%d" (show $ book) tome rank
  where
    PrimaryKey {book, tome, rank} = get a

relativePath :: Has PrimaryKey a => a -> String -> FilePath
relativePath a extension =
  (show book) </> ("T" <> show tome) </> (show rank) <.> extension
  where
    PrimaryKey {book, tome, rank} = get a

instance HasDefaultHeader PrimaryKey where
  defaultFields = DefaultFields [ "book", "tome", "rank" ]
