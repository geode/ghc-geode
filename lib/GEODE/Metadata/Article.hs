{-# LANGUAGE DeriveGeneric, NamedFieldPuns #-}
module GEODE.Metadata.Article
  ( Article(tome, name, headWord, rank, page) ) where

import Data.Csv (FromRecord(..), ToNamedRecord(..), ToRecord(..))
import GEODE.Metadata.Types (Book)
import GEODE.Metadata.Projector (InFile(..), Unique(..))
import qualified GEODE.Metadata.Projector as Projector (FromBook(..))
import Data.Text (Text, unpack)
import GHC.Generics (Generic)
import Text.Printf (printf)

data Article = Article
  { book :: Book
  , tome :: Int
  , name :: Text
  , headWord :: Text
  , rank :: Int 
  , page :: Int } deriving (Generic, Show)

instance FromRecord Article
instance ToRecord Article
instance ToNamedRecord Article

instance Unique Article where
  uid (Article {book, tome, name}) =
    printf "%s_%d_%s" (show book) tome (unpack name)

instance Projector.FromBook Article where
  book = book

instance InFile Article where
  relativePath (Article {book, tome, name}) =
    printf "%s/T%d/%s" (show book) tome (unpack name)

