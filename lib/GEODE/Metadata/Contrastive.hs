{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
module GEODE.Metadata.Contrastive
  ( Contrastive(..)
  , MultiText(..)
  , formatList ) where

import Data.Csv
  ( FromField(..), FromNamedRecord(..), ToNamedRecord(..), ToField(..) )
import Data.Text (Text, intercalate, splitOn, uncons, unsnoc)
import GEODE.Metadata.TSV.Header (DefaultFields(..), HasDefaultHeader(..))
import GHC.Generics (Generic)

newtype MultiText = MultiText
  { getList :: [Text] } deriving (Show)

formatList :: MultiText -> Text
formatList = colonFormat . getList
  where
    colonFormat ts = ":" <> intercalate ":" ts <> ":"

instance FromField MultiText where
  parseField f = parseField f >>= checkAndSplit
    where
      popBoundaries t0 = do
        (firstChar,t1) <- uncons t0
        (middle,lastChar) <- unsnoc t1
        pure (firstChar,middle,lastChar)
      checkAndSplit t =
        case popBoundaries t of
          Just (':',"",':') -> pure $ MultiText []
          Just (':',fields,':') -> pure.MultiText $ splitOn ":" fields
          _ -> mempty

instance ToField MultiText where
  toField = toField . formatList

data Contrastive = Contrastive
  { authors :: MultiText
  , domains :: MultiText
  , subCorpus :: MultiText } deriving (Generic, Show)

instance FromNamedRecord Contrastive
instance ToNamedRecord Contrastive

instance HasDefaultHeader Contrastive where
  defaultFields = DefaultFields [ "authors", "domains", "subCorpus" ]
