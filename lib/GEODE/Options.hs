{-# LANGUAGE NamedFieldPuns #-}
module GEODE.Options
  ( Input(..)
  , IOConfig(..)
  , Output(..)
  , input
  , ioConfig
  , output
  , xmlOutput ) where

import Control.Applicative ((<|>))
import Data.List.NonEmpty (NonEmpty(..))
import Options.Applicative
  ( Parser, argument, flag', help, long, metavar, short, str, strOption, value )
import Options.Applicative.NonEmpty (some1)
import Text.Printf (printf)

data Input = StdIn | File FilePath

input :: String -> Parser Input
input stdinSemantics =
  argument
    (File <$> str)
    ( value StdIn
    <> metavar "INPUT_FILE"
    <> help (printf "path of the file to process (%s)" stdinSemantics) )

data Output = Metadata | TextRoot FilePath | XMLRoot FilePath

xmlOutput :: Parser FilePath
xmlOutput =
  strOption
    ( long "xml-root" <> metavar "DIRECTORY" <> short 'x'
    <> help "Root path where to output XML files" )

output :: Parser Output
output =
    flag' Metadata ( long "metadata"
                   <> short 'm'
                   <> help "Print metadata for splitted files on stdout" )
  <|> (TextRoot <$> strOption
        ( long "text-root"
        <> metavar "DIRECTORY"
        <> short 't'
        <> help "Root path where to output text files" ))
  <|> (XMLRoot <$> xmlOutput)

data IOConfig = IOConfig
  { from :: Input
  , to :: NonEmpty Output }

ioConfig :: String -> Parser IOConfig
ioConfig stdinSemantics = IOConfig
  <$> input stdinSemantics
  <*> some1 output
